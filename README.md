# nfacctd-amqp

nfacctd collects NetFlow, parse and send as json to AMQP message broker, like RabbitMQ.

## Usage Example

Build
```shell
docker build -t nfacctd-amqp:local .
```

Start nfacctd on udp port 2100 (default) to listen neflow packets.
```shell
docker run -d --network host nfacctd-amqp:local
```

## Environment Variables

* **`NFACCTD_CONFIG`**: ["/path/to/nfacctd.conf"] Default "/nfacctd.conf". Path to config inside docker container.
* **`NFACCTD_DEBUG`**: ["true"|"false"] Defailt "false". Set "debug" value in config file. Enables debug.
* **`NFACCTD_LISTEN`**: [port] Default 2100. Set "nfacctd_port" value in config file. Defines the UDP port where to bind nfacctd.
* **`NFACCTD_AGGREGATE`**: ["comma-separated list of aggregate fields"] Default "src_mac,dst_mac,vlan,src_host,dst_host,proto,src_port,dst_port,tcpflags,timestamp_start,timestamp_end,timestamp_arrival,export_proto_seqno,export_proto_version". Set "aggregate" value in config file. Aggregate captured traffic data by selecting the specified set of primitives.
* **`NFACCTD_AMQP_HOST`**: ["hostname|IP"] Default "rabbitmq". Set "amqp_host" value in config file. Defines the AMQP/RabbitMQ broker IP.
* **`NFACCTD_AMQP_VHOST`** ["path"] Default "/". Set "amqp_vhost" value in config file. Defines the AMQP/RabbitMQ server virtual host.
* **`NFACCTD_AMQP_USER`**: ["username"] Default "guest". Set "amqp_user" value in config file. Defines the username to use when connecting to the AMQP/RabbitMQ server.
* **`NFACCTD_AMQP_PASSWD`**: ["password"] Default "guest". Set "amqp_passwd" value in config file. Defines the password to use when connecting to the server.
* **`NFACCTD_AMQP_ROUTING_KEY`**: ["routing key name"] Default "acct". Set "amqp_routing_key" value in config file. Name of the AMQP routing key to attach to published data.
* **`NFACCTD_AMQP_EXCHANGE`**: ["exchange name"] Default "pmacct". Set "amqp_exchange" value in config file. Name of the AMQP exchange to publish data.
* **`NFACCTD_AMQP_EXCHANGE_TYPE`**: ["direct|fanout|topic"] Default "direct". Set "amqp_exchange_type" value in config file. Type of the AMQP exchange to publish data to.
* **`NFACCTD_AMQP_PERSISTENT`**: ["true"|"false"] Default "false". Set "amqp_persistent_msg" value in config file. Marks messages as persistent and sets Exchange as durable so to prevent data loss if a RabbitMQ server restarts (it will still be consumer responsibility to declare the queue durable).
* **`NFACCTD_AMQP_FRAME_MAX`**: [4096-2147483647] Default 131072. Set "amqp_frame_max" value in config file. Defines the maximum size, in bytes, of an AMQP frame on the wire to request of the broker for the connection.
* **`NFACCTD_AMQP_HEARTBEAT_INTERVAL`**: [0-2147483647] Default 0(disabled). Set "amqp_heartbeat_interval" value in config file. Defines the heartbeat interval in order to detect general failures of the RabbitMQ server.
* **`NFACCTD_PLUGIN_PIPE_SIZE_AMQP`**: [size in bytes] Default 4096000(4MB). Set "plugin_pipe_size[amqp]" value in config file. This directive sets the total size, in bytes, of queue between core nfacctd process and output plugin.
* **`NFACCTD_PLUGIN_BUFFER_SIZE`**: [size in bytes] Default 40960. Set "plugin_buffer_size" value in config file. By defining the transfer buffer size, in bytes, this directive enables buffering of data transfers between core process and active plugins. Once a buffer is filled, it is delivered to the plugin.
* **`NFACCTD_TEMPLATES_FILE`**: ["path to templates cache file"] Default "/var/lib/pmacct/nfacctd.templates-cache". Set "nfacctd_templates_file" value in config file. Full pathname to a file containing serialized templates data from previous nfacctd use.

FROM ubuntu:18.04
LABEL maintainer "Aleksey Koloskov <vsyscoder@gmail.com>"

ENV NFACCTD_CONFIG="/nfacctd.conf" \
    NFACCTD_DEBUG="false" \
    NFACCTD_LISTEN=2100 \
    NFACCTD_AGGREGATE="src_mac,dst_mac,vlan,src_host,dst_host,proto,src_port,dst_port,tcpflags,timestamp_start,timestamp_end,timestamp_arrival,export_proto_seqno,export_proto_version" \
    NFACCTD_AMQP_HOST="rabbitmq" \
    NFACCTD_AMQP_VHOST="/" \
    NFACCTD_AMQP_USER="guest" \
    NFACCTD_AMQP_PASSWD="guest" \
    NFACCTD_AMQP_ROUTING_KEY="acct" \
    NFACCTD_AMQP_EXCHANGE="pmacct" \
    NFACCTD_AMQP_EXCHANGE_TYPE="direct" \
    NFACCTD_AMQP_PERSISTENT="false" \
    NFACCTD_AMQP_FRAME_MAX=131072 \
    NFACCTD_AMQP_HEARTBEAT_INTERVAL=0 \
    NFACCTD_PLUGIN_PIPE_SIZE_AMQP=4096000 \
    NFACCTD_PLUGIN_BUFFER_SIZE=40960 \
    NFACCTD_TEMPLATES_FILE="/var/lib/pmacct/nfacctd.templates-cache"

COPY nfacctd.conf.template /nfacctd.conf.template
COPY run.sh /run.sh

RUN chmod +x /run.sh

RUN apt-get -y update && \
    apt-get -y install pmacct gettext-base

EXPOSE $NFACCTD_LISTEN

CMD ["/run.sh"]

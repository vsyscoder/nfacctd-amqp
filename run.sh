#!/bin/sh

envsubst < nfacctd.conf.template > nfacctd.conf
CONFIG="${NFACCTD_CONFIG:-nfacctd.conf}"

cat nfacctd.conf

/usr/sbin/nfacctd -f $CONFIG $@
